#Ejercicio 3: Escribe un programa que solicite una puntuación entre 0.0 y 1.0. Si la
#puntuación está fuera de ese rango, muestra un mensaje de error. Si la puntuación
#está entre 0.0 y 1.0, muestra la caliﬁcación usando la tabla siguiente:
#Puntuación Calificación
#>= 0.9 Sobresaliente
#>= 0.8 Notable
#>= 0.7 Bien
#>= 0.6 Suficiente
#< 0.6 Insuficiente

try:
    pun=float(input(" ingrese puntuacion: "))
    if pun >= 0.0 and pun <= 1:
        if pun >= 0.9:
            print(" sobresaliente")
        else:
            if pun >= 0.8:
                print("notable")
            else:
                if pun >= 0.7:
                    print("bien")
                else:
                    if pun >= 0.6:
                        print("suficiente")
                    else:
                        if pun < 0.6:
                            print(" insuficiente")
    else:
        print(" puntuacion incorrecta")
except:
    print(" puntuacion incorrecta")